# profile-sketcher
Diego Garay-Ruiz, November 2021

Institute of Chemical Research of Catalonia

Prof. Carles Bo Research Group

## What's this
profile-sketcher is a Python tool to generate energy profiles in CDXML format, as ChemDraw-compliant objects, in a fully automated manner from plain CSV files.

The resulting energy profiles can then be fully edited inside ChemDraw, modifying the aesthetics or including chemical structures.

Additional functions are included to produce Matplotlib-based plots instead of the CDXML files, with draggable labels for customization.

## Package structure & dependencies
- profsketcher.py. Main library.
- profsketch_cmd.py. Simple run script to call the program from command line.

Requires:
+ numpy
+ matplotlib

## Input format
profile-sketcher expects CSV files, separated by either "," or ";", with *four* columns

LABEL: string, tag for the species \
ENERGY: float, energy value for the species \
STAGE: integer, order of the species in the mechanism.  \
PATHWAY: string, alphabetic label (A,B,C...) stating in which mechanism(s) this species participates.
When there is a single mechanism, a single arbitrary label can be used. When there are several, species can be either repeated or specified with joined pathway labels such as *AB*, *ABC*... to simplify the file 

This Python3 program takes a CSV file with info of a reaction mechanism and creates a CDXML file
(ChemDraw-readable) with the corresponding profile.

## Installation instructions
A basic installer from setuptools, `setup.py` is provided. Inside the main folder, run:

```
pip install -e .
```

to install the package in *editable* mode: thus, all changes in the **py** files of the three main modules will be automatically available.

## Running the program
Via command-line:
`python profsketch_cmd.py INPUT_FILE.CSV`

### Options for command-line runs

*--help*. To print available options. \
*--label_spacing*. Separation between label and energy, in cm (0.3 cm). \
*--digits*: digits after decimal point in energy labels (1). \
*--xscale*: X-scaling, for horizontal axis (30 units). \
*--yscale*: Y-scaling, for vertical axis (30 units). \
*--pyplot*: Allow to generate SVG plot and interactive Matplotlib session \
*--cmap*. Name of a Matplotlib colormap to color the different mechanisms (tab20). \
*--colorlist*. List of RGB hex color codes for custom coloring. \
*--line_spacing*. Relative X-spacing between consecutive species (1 unit)
