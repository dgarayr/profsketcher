from setuptools import setup,find_packages
setup(name='ProfSketcher',
      version='2.0',
      description="Generate ChemDraw-compatible reaction energy profiles",
      author="Diego Garay-Ruiz",
      author_email="dgaray@iciq.es",
      packages=find_packages())
