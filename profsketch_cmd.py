'''April 2020, Diego Garay-Ruiz.
Wrapper script to launch profile_sketcher from command line
'''
import profsketcher as pskt
arg_array = pskt.initialize_commandline()
reaction_data = pskt.data_reader(arg_array)
pskt.cdxml_gen(reaction_data,arg_array)
if (arg_array.pyplotflag):
        pskt.mpl_plotter(reaction_data,arg_array)
