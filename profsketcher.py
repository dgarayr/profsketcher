'''
================PROFILE SKETCHER========================
Diego Garay-Ruiz, Spring 2019
Take label/energy/stage/pathway as input and create a energy diagram for 
a chemical reaction in CDXML format for further editing in ChemDraw

Winter 2019:
Patched file writing to avoid unnecessary .write() calls: instead, dump to
a string variable and only write each block

Added --pyplot flag to generate a figure with Matplotlib apart from the
CDXML profile

February 2020:
Attempted to generalize implementation: wrap up in functions.

February 2021:
General cleanup!
October 2021:
Reorganize support, drop DataFrames?
'''
import numpy as np
import copy
from operator import itemgetter
from collections import defaultdict
import argparse
import matplotlib.cm
import matplotlib.pyplot as plt
import matplotlib.collections
import sys

class ParameterContainer:
	'''Class holding the required parameters for CDXML file generation.
	Can be generated from scratch, via initialize_py(), or through argument parsing via 
	initialize_commandline()
	The attributes:
	- filename. Name of the CSV file to read.
	- spacing. Float, cm between label and energy.
	- digits. Integer, no. of digits for energy labels.
	- xscale. Float, scaling factor for the X-dimension
	- yscale. Float, scaling factor for the Y-dimension
	- pyplot. Boolean, if True generate a Matplotlib figure with the CDXML.
	- ydispl. Float, fixed in the initializing functions for consistency with ChemDraw specs.
	- cmap. String, name of the Matplotlib colormap to use
	'''
	def __init__(self):
		self.filename = None
		self.label_spacing = 0.0
		self.line_spacing = 1.0
		self.spacing_adjusted = 0.0
		self.digits = 0
		self.xscale = 0.0
		self.yscale = 0.0
		self.pyplotflag = False
		self.ydispl = 0
		self.cmap = "tab10"
		self.colorlist = ""
		### Additional parameters for Py-based plotting, ignored in CDXML
		self.lw1 = 1
		self.lw2 = 3

	def initialize_py(self,filename="default_file",label_spacing=0.3,digits=1,xscale=30,yscale=5,line_spacing=1,pyplotflag=False):
		'''Initializing function to run inside a Py session or another script. Provide
		initialization values inside the program. All but the filename have defaults. 
		Hard-coded values: adjustment of spacing to ChemDraw standards and y-axis displacement.
		Should not need to modify these.
		Input:
		- filename. String, name of the CSV file to read.
		- spacing. Float, separation between label and energy in cm.
		- digits. Integer, no. of digits for energy labels.
		- xscale. Integer, separation in x-axis -> default 30 units.
		- yscale. Integer, separation in y-axis -> default 5 units.
		- pyplotflag. Boolean, if True, provide interactive Matplotlib plot together with CDXML.
		Output:
		- arg_array. ParameterContainer() obj. containing all input parameters.
		'''

		#Assign values
		self.filename = filename
		self.label_spacing = label_spacing
		self.spacing_adjusted = label_spacing*28.25 # consistency with ChemDraw
		self.digits = digits
		self.xscale = xscale
		self.yscale = yscale
		self.pyplot = pyplotflag
		self.line_spacing = line_spacing
		#y-displacement (hardcoded)
		self.ydispl = 200.0
		return self


#INITIALIZATION: 

def initialize_commandline():
	'''To call the program from the command line, instantiate an ArgumentParser() object
	to handle STDIN-provided parameters. All but the filename have defaults. Exit if no
	filename is provided.

	Hard-coded values: adjustment of spacing to ChemDraw standards and y-axis displacement.
	Should not need to modify these.
	
	All arguments are passed to a ParameterContainer object
	'''
	#Create a parsing_args object to get required data
	parsing_args = argparse.ArgumentParser()
	parsing_args.add_argument("filename",help="Name of the CSV file to read",nargs="?")
	parsing_args.add_argument("--label_spacing",help="Spacing (in cm) between label and energy (def. 0.3 cm)",type=float,default=0.3)
	parsing_args.add_argument("--digits",help="No. of digits for energy plotting (def. 1)",type=int,default=1)
	parsing_args.add_argument("--xscale",help="X-scaling factor (def. 30 units)",type=float,default=30)
	parsing_args.add_argument("--yscale",help="Y-scaling factor (def. 5 units)",type=float,default=5)
	parsing_args.add_argument("--pyplot", help="Create Matplotlib plot with the CDXML file, Y/N",action="store_true")
	parsing_args.add_argument("--cmap", help="Name of the Matplotlib colormap used for the CDXML (def. is tab20)",type=str,default="tab20")
	parsing_args.add_argument("--colorlist",help="List of comma-separated custom RGB hex color codes for the CDXML",type=str,default="")
	parsing_args.add_argument("--line_spacing",help="Relative spacing between consecutive species in the profile (def. 1 unit)",type=float,default=1)
	parsing_args.add_argument("--skip_labels",help="Do not add labels more than once",action="store_true")
	##All default values are set here:
	#0.3 cm for spacing, 1 digit for energy labels, 30 unit xscale and 5 unit yscale
	parsed_arguments = parsing_args.parse_args()
	if not parsed_arguments.filename:
		print("Insert a CSV file to read")
		print("Required fields: label, energy, stage and pathway")
		print("=============")
		parsing_args.print_help()
		sys.exit()
	# Instantiate the ParameterContainer
	arg_array = ParameterContainer()
	# Set all parameters
	par_list = ["filename","label_spacing","digits","xscale","yscale","cmap","colorlist","line_spacing","skip_labels"]
	for par in par_list:
		par_value = getattr(parsed_arguments,par)
		setattr(arg_array,par,par_value)

	# Additional user prompt for customization
	# Label/energy value spacing, in cm
	# Line spacing: multiply by 28.25 to get values according to what ChemDraw does
	#use arg_array as main object to communicate data
	arg_array.spacing_adjusted = arg_array.label_spacing*28.25
	#boolean for pyplot flag
	arg_array.pyplotflag = parsed_arguments.pyplot
	#Y-axis displacement
	arg_array.ydispl = 200.0
	return arg_array

class ProfileData:
	'''Class to store information about profiles'''
	def __init__(self):
		self.Label = []
		self.Energy = []
		self.Stage = []
		self.Pathway = []
		self.ColumnNames = ["Label","Energy","Stage","Pathway"]

	def read_list(self,in_list):
		'''From a list of lists containing [Label, Energy, Stage, Pathway]
		entries, extract fields & format accordingly'''
		self.Label = [item[0] for item in in_list]
		self.Energy = [float(item[1]) for item in in_list]
		self.Stage = [int(item[2]) for item in in_list]
		self.Pathway = [item[3].strip() for item in in_list]
		return self

	def read_long_list(self,labels,energies,stages,pathways):
		'''Read individual lists for the required properties'''
		self.Label = labels
		self.Energy = energies
		self.Stage = stages
		self.Pathway = pathways
		return self
	
	def read_simple_list(self,labels,energies,path_tag="A"):
		'''From a simple profile description (labels and energies), define stages
		and pathways automatically'''
		stages = [ii+1 for ii in range(len(labels))]
		pathways = [path_tag for ii in range(len(labels))]
		self.read_long_list(labels,energies,stages,pathways)
		return self

	def filter_profile(self,mask):
		# Extract elements from all lists according to a boolean mask
		filtered_prof = []
		for colname in self.ColumnNames:
			column = getattr(self,colname)
			filter_column = [item for keep,item in zip(mask,column) if keep]
			filtered_prof.append(filter_column)
			#setattr(self,colname,filter_column)
		# Convert to records and use this to build a new ProfileData element
		records = self.build_records(filtered_prof)
		filt_prof = ProfileData().read_list(records)
		return filt_prof

	def sort_profile(self,sorting_col="Stage",inplace=True):
		# Sort elements according to the sorting column
		# Get the corresponding mask, using enumerate to get indices and itmegetter to sort by value and not by ndx
		column = getattr(self,sorting_col)
		sorted_ndx = [entry[0] for entry in sorted(enumerate(column),key=itemgetter(1))]
		sorted_profile = []
		for colname in self.ColumnNames:
			column = getattr(self,colname)
			sorted_column = [column[ii] for ii in sorted_ndx]
			if (inplace):
				setattr(self,colname,sorted_column)
			else:
				sorted_profile.append(column)
		if (inplace):
			return None
		else:
			records = self.build_records(sorted_profile)
			sort_prof = ProfileData().read_list(records)
			return sort_prof

	def build_records(self,profile_list):
		'''Switch between 4-list based representation (labels, energies, stages, pathways) to
		a single list of [Label, Energy, Stage, Pathway] records, compatible with self.read_list()'''
		labels,energies,stages,pathways = profile_list
		prof_records = [[lab,e,stg,pth] for lab,e,stg,pth in zip(labels,energies,stages,pathways)]
		return prof_records
	
	def pathway_splitter(self):
		'''Process the current ProfileData with label/energy/stage/pathway information
		and separate it into a list of individual mechanisms (according to
		pathway information)
		Output:
		- mechanisms_list. List of ProfileData objects for every individual reaction mechanism,
		according to the corresponding pathway field.
		'''
		# ===PATHWAY ANALYSIS AND SEPARATION
		# Get the number of different paths in the data: one-letter codes A to Z
		all_path_letters = [letter for item in self.Pathway for letter in item]
		all_path_labels = set(all_path_letters)
		num_paths = len(all_path_labels)
		# Maximum stage and furthest x-coord in plot
		max_stage = max(self.Stage)
		max_x = 2*max_stage + 1 

		# Separation of mechanisms: consider all different path labels (different mechanisms) and check
		# the entries matching them
		mechanisms = []
		for path_label in sorted(all_path_labels):
			# Create the boolean mask and build a new profile
			mask = [path_label in tag for tag in self.Pathway]
			prof = self.filter_profile(mask)
			# And reassign the corresponding pathway
			prof.Pathway = [path_label for tag in prof.Pathway]
			mechanisms.append(prof)       
		return mechanisms

	def dump_lists(self):
		return self.Label,self.Energy,self.Stage,self.Pathway
	
	def __len__(self):
		return len(self.Label)

	def __getitem__(self,ii):
		entry = [getattr(self,colname)[ii] for colname in self.ColumnNames]
		return entry

	def __repr__(self):
		# Do pretty-printing by looping through all entries
		str_block = ""
		for ii in range(len(self)):
			str_block += "%30s %10.2f %8d %15s\n" % tuple(self[ii])
		return str_block

	def __add__(self,other):
		if (type(other) != type(self)):
			raise TypeError
		sum_profs = ProfileData()
		sum_profs.Label = self.Label + other.Label
		sum_profs.Energy = self.Energy + other.Energy
		sum_profs.Stage = self.Stage + other.Stage
		sum_profs.Pathway = self.Pathway + other.Pathway
		return sum_profs

	def __radd__(self,other):
		# allow to handle zero sums to be able to sum across iterables
		if (other == 0):
			return self
		else:
			return self.__add__(other)
	
	def max(self):
		max_ndx = np.argmax(self.Energy)
		return self[max_ndx]
		
	def min(self):
		min_ndx = np.argmin(self.Energy)
		return self[min_ndx]

def data_reader(arg_array):
	'''Read a valid CSV input file, whose filename should be stored 
	as .filename attribute in an ArgumentParser() or ParameterContainer()
	Input file must contain:
		Label
		Energy
		Stage
		Pathway
	Input:
	- arg_array. Initialized ArgumentParser or ParameterContainer
	Output:
	- profile_data. Filled ProfileData object 
   ''' 
	#Read data from input into a DataFrame
	infile = arg_array.filename
	with open(infile,"r") as fcsv:
		# detect delimiter: , or ;. This also skips 1st line (header)
		if (",") in fcsv.readline():
			delim = ","
		else:
			delim = ";"
		dump = [entry.split(delim) for entry in fcsv.readlines()]
	profile_data = ProfileData()
	profile_data.read_list(dump)
	return profile_data

def pathway_analyzer(profile_data):
	'''Process a ProfileData with label/energy/stage/pathway information
	and separate it into a list of individual mechanisms (according to
	pathway information). Wrapper for consistency.
	Input:
	- profile_data. Filled ProfileData object 
	Output:
	- mechanisms_list. List of ProfileData objects for every individual reaction mechanism,
	according to the corresponding pathway field.
	'''
	mechanisms = profile_data.pathway_splitter()
	return mechanisms

### Block generators: a function for every block type 

def colortable_block():
	'''Generate a tagged (XML-based) block of text for the colortable,
	adapted from ChemDraw defaults.
	Input:
	- None
	Output:
	- colortable. String, newline-separated block of text'''
	# Might add a more rational color support later on!
	##Colortable, taken from ChemDraw project
	colortable = ""
	colortable += ("<colortable>\n")
	colortable += ("<color r=\"1\" g=\"1\" b=\"1\"/>\n")
	colortable += ("<color r=\"0\" g=\"0\" b=\"0\"/>\n")
	###above -> black & white -> foreground and background
	colortable += ("<color r=\"1\" g=\"0\" b=\"0\"/>\n")
	colortable += ("<color r=\"0\" g=\"1\" b=\"0\"/>\n")
	colortable += ("<color r=\"0\" g=\"1\" b=\"1\"/>\n")
	colortable += ("<color r=\"0\" g=\"0\" b=\"1\"/>\n")
	colortable += ("<color r=\"1\" g=\"1\" b=\"0\"/>\n")
	colortable += ("<color r=\"1\" g=\"0\" b=\"1\"/>\n")
	colortable += ("<color r=\"0.5020\" g=\"0\" b=\"1\"/>\n")
	colortable += ("<color r=\"1\" g=\"0\" b=\"0\"/>\n")
	colortable += ("<color r=\"0.7529\" g=\"0.7529\" b=\"0.7529\"/>\n")
	colortable += ("</colortable>\n")
	return colortable

def color_formatter(rgb_hex,alpha=1.0):
	'''Transform RGB hex into a valid RGBA tuple of  4 entries from 0 to 1
	Input:
	- rgb_hex. String, RGB hexadecimal code
	- alpha. Float, alpha value'''
	if ("#" in rgb_hex):
		rgb_hex = rgb_hex[1:]
	r = rgb_hex[0:2]
	g = rgb_hex[2:4]
	b = rgb_hex[4:]
	out_tuple = (int(r,16)/255,int(g,16)/255,int(b,16)/255,alpha)
	return out_tuple

def color_formatter_back(rgb_tuple):
	'''Transform RGBA tuple to RGB hex
	Input:
	- rgb_tuple. Tuple, containing 4 integers with 0-1 RGBA values
	Output:
	- rgb_hex. String, RGB hexadecimal code'''
	hex_list = [hex(int(255*val))[2:].upper() for val in rgb_tuple]
	return "#" + "".join(hex_list)

def colortable_mpl(cmap,custom_colors=[]):
	'''Generate a tagged (XML-based) block of text for the colortable 
	based on a matplotlib colormap. Always use first black & white for
	foreground and background, then add the colormap.
	Input:
	- cmap. Matplotlib colormap
	- custom_colors. List of RGB codes for custom colors that will be
	put BEFORE the requested colormap
	Output:
	- colortable. String, newline-separated block of text
	'''
		
	Ncolors = 10
	color_list = [cmap(xval) for xval in np.linspace(0,1,Ncolors)]
	# convert the RGB tuples to tabs
	format_tag = "<color r=\"%.4f\" g=\"%.4f\" b=\"%.4f\"/>\n" 
	# add black and white tuples too
	default_colors = [(1.0, 1.0, 1.0, 1.0),(0.0, 0.0, 0.0, 1.0)]

	# Check custom colors and add them
	if (custom_colors):
		color_tuples = [color_formatter(code) for code in custom_colors]
		default_colors += color_tuples
	# Define the XML block
	colortable = "<colortable>\n"
	for ctuple in (default_colors + color_list):
		colortable += format_tag % ctuple[0:3]
	colortable += "</colortable>\n"
	return colortable

def node_block(ncount,xpos,ypos):
	'''Generate a tagged (XML-based) block of text for defining the
	nodes in the CDXML file
	Input:
	- ncount. Integer, counter for the node being generated.
	- xpos, ypos. Floats, positions in the X and Y axes.
	Output:
	- node. String, newline-separated block of text'''
	node = ''
	node +=("<n \n")
	node +=("id=\"%d\" \n" % ncount)
	node +=("p=\"%.2f  %.2f\" \n" % (xpos,ypos))
	node += ("/> \n")
	return node

def label_block(xpos,ypos,text_type,label_text,arg_array,colorid):
	'''Generate a tagged (XML-based) block of text for defining the labels (text & position)
	in the CDXML file.
	Input:
	- xpos,ypos. Floats, positions for the tag.
	- text_type. String, either "label" or "energy", to select the type of label.
	- label_text. String or float, depending on text_type: energies are passed as floats and formatted inside.
	- arg_array. ParameterContainer with values for drawing variables.
	- colorid. Integer, index of the color in the colortable.
	Output:
	- label. String, newline-separated block of text'''

	label=''
	if (text_type == "label"):
		#Textbox for species' label
		label += ("<t \n")
		label += ("Justification=\"Center\"\n")
		label += ("LineHeight=\"%.2f\"\n" % arg_array.spacing_adjusted)
		label += ("p=\"%.2f  %.2f\"\n" % (xpos,ypos))
		#Sign of pos_y has to be changed to have proper representation
		label += ("> \n")
		#Add label string
		label += ("<s>%s</s></t>\n" % label_text)
		#Set this position as occupied
		#used_label_pos.add(tuplepos)
	elif (text_type == "energy"):
		#Textbox for species' energy
		label += ("<t \n")
		label += ("Justification=\"Center\"\n")
		label += ("p=\"%.2f  %.2f\"\n" % (xpos,ypos))
		#Sign of pos_y has to be changed to have proper representation
		label += ("> \n")
		#Energy is colored by mechanism. We need to include font=N for the color to work.
		#By default, font=4 is Times New Roman
		format_string=("<s color=\"%d\" font=\"4\">" % colorid) + "%." + ("%df</s></t>\n" % arg_array.digits)
		label += (format_string % label_text)
	return label

def line_block(ncount,begin,end,colorid,species_flag):
	'''Generate a tagged (XML-based) block of text for defining the
	lines for species and the connecting lines to be drawn in the CDXML file
	Input:
	- ncount. Integer, counter for the object being generated.
	- begin, end. Integers, indices to be connected.
	- colorid. Integer, index of the color in the colortable.
	- species_flag. Boolean, True for lines corresponding to species and False for connectors
	- xpos, ypos. Floats, positions in the X and Y axes.
	Output:
	- line. String, newline-separated block of text
	'''
	line = ""
	line += ("<b \n")
	line += ("id=\"%d\" \n" % ncount)
	line += ("B=\"%d\" \n" % begin)
	line += ("E=\"%d\" \n" % end)
	#Species' lines shall be bold
	if (species_flag):
		line += ("Display=\"Bold\" \n")
		line += ("color=\"%d\" \n" % colorid)
	else:
		#Additional options could be used here to affect connecting lines
		line += ("color=\"%d\" \n" % colorid)
	line += ("/> \n")
	return line

def xy_array_gen(mech_data,line_spacing=1.0):
	'''Transform the ProfileData representing a mechanism into a 2D np.array for stage and energy, duplicating all
	entries to allow XY plotting
	Input:
	- mech_data. ProfileData for an individual mechanism.
	- line_spacing. Float, value, relative to 1, to be between two consecutive lines in the profile.
	Output: 
	- xy_array. np.array of floats, of size 2Nx2
	- label_list. List of strings containing all labels in the mech, duplicated to match the array'''
	mech_data.sort_profile("Stage")

	# Iterate and duplicate entries to get the energy levels, through listcomps
	xvals = [item for ii in mech_data.Stage for item in [2*ii,2*ii+1]]
	yvals = [item for evalue in mech_data.Energy for item in [evalue]*2]
	label_list = [item for label in mech_data.Label for item in [label]*2]
	# Generate the array & prepare scaling
	xy_array = np.array([xvals,yvals]).T
	if (np.abs(line_spacing - 1.0) > 1E-4):
		xy_array = space_adjusting(xy_array,line_spacing)
	return xy_array,label_list

def space_adjusting(xy_array,line_spacing):
	'''Rescaling of profiles to modify the space between consecutive lines in the profile,
	keeping all profile lines to rel. length 1
	Input:
	- xy_array. np.array of floats of size Nx2 representing stage and energy for a mechanism
	- line_spacing. Float, value, relative to 1, to be between two consecutive lines in the profile.
	Output:
	- scaled_array. np.array of floats, size Nx2, with rescaled stages.
	'''
	# Number of rows in the array (N) must be even. Start modifying from 3rd entry (the two first are the 1st line)
	scaled_array = xy_array.copy()
	start_x = scaled_array[1,:]
	for ii in range(2,scaled_array.shape[0]):
		# if the index is even, change the spacing. If odd, put at 1-dist from the previous
		if (ii % 2 == 0):
			set_spacing = line_spacing
		else:
			set_spacing = 1
		scaled_array[ii,0] = scaled_array[ii-1,0] + set_spacing
	return scaled_array

def node_iterator(xy_array,arg_array,counter):
	'''Go along an array with X,Y positions for the plot and generate the corresponding
	CDXML elements for the nodes used for later drawing
	Input:
	- xy_array. np.array of floats of size 2Nx2 with stage & energy information
	- arg_array. ParameterContainer() with plotting information
	- counter. Integer, used for node numbering
	Output:
	- list_nodes. List of lists containing nodes defined by counter (int) and X and Y coordinates (float)
	- counter. Integer, updated value for node numbering.
	'''
	# Start by rescaling if requested, checking if the scaling arg. is distinct from 1
	list_nodes = []
	for nodepair in xy_array:
		pos_x = nodepair[0]*arg_array.xscale
		pos_y = nodepair[1]*arg_array.yscale
		# Save information in a list: counter, X and Y positions. Round positions to three digits for simplicity. Round positions to three digits for simplicity
		xloc,yloc = [round(val,3) for val in (pos_x,-pos_y+arg_array.ydispl)]
		node = [counter,xloc,yloc]
		list_nodes.append(node)
		counter += 1
	return list_nodes,counter

def node_bunch_tagger(node_bunch_list):
	'''From a list of lists containing counter, X and Y, for a set of nodes generate the CDXML element.#!/usr/bin/env python
	Input:
	- ndlist. List of lists containing counter (int) and X and Y (float) positions for a given node
	Output:
	- all_node_block. String, newline-separated block of text.
	'''
	tagblocklist = [node_block(*tuple(ndlist)) for ndlist in node_bunch_list]
	all_node_block = "".join(tagblocklist)
	return all_node_block

def label_iterator(xy_array,arg_array,label_list,colorid,used_label_pos=None,
				   skip_rep_labels=False,used_labels=[]):
	'''Process and position energies and names as labels in the CDXML file
	Input:
	- xy_array. np.array of floats of size Nx2 with stage & energy information
	- arg_array. ParameterContainer() with plotting information
	- label_list. List of strings with names for species in the mechanism, duplicated to match xy_array
	- colorid. Integer, identifier for the color being used (for energies)
	- used_label_pos. Set of tuples with used x & y positions. If None, start as empty set.
	- skip_rep_labels. Boolean, if True, do not add labels that have already appeared anywhere else.
	- used_labels. List of strings containing already used labels.
	Output:
	- all_label_block. String, newline-separated block of text.
	'''
	# Modified used_label_pos for consistency upon multiple calls
	if (not used_label_pos):
		used_label_pos = set()
	all_label_block = ""
	# Using indices makes it easier to skip repeated entries and access other rows
	for k in range(0,len(label_list),2):
		nodepair = xy_array[k,:]
		pos_x_label = nodepair[0]*arg_array.xscale + arg_array.xscale/2         #shift to the right
		pos_y_label = nodepair[1]*arg_array.yscale + arg_array.yscale/1.5	        #shift below
		tuplepos = (pos_x_label,-pos_y_label+arg_array.ydispl)                #general y-shift
		label = label_list[k]
		# Skip already occupied positions
		if (tuplepos in used_label_pos):
			continue
					
		# Put labels above and below the species' line.
		xpos = tuplepos[0]
		current_e = nodepair[1]
		pos_above,pos_below = [tuplepos[1],tuplepos[1]+2*arg_array.spacing_adjusted]
		# Detect TS criterion, skipping the extremes 
		try:
			prev_e = xy_array[k-2,1]
			next_e = xy_array[k+2,1]
			#TS criterion -> energy is above the prev. and the next.
			ts_flag = (current_e > prev_e and current_e > next_e)
		except:
			ts_flag = False
		if (ts_flag):
			#Transition state. Label above, energy below.
			yposL,yposE = [pos_above,pos_below]
		else:
			#Intermediate. Label below, energy above
			yposL,yposE = [pos_below,pos_above]
		# Label block generation: keep track of tags and do not add them if skip_rep_labels is used and are repeated
		skip_tag_label = (label in used_labels and skip_rep_labels)
		if (not skip_tag_label):
			used_labels.append(label)
			all_label_block += label_block(xpos,yposL,'label',label,arg_array,colorid)
			used_label_pos.add(tuplepos)
		all_label_block += label_block(xpos,yposE,'energy',current_e,arg_array,colorid)
		used_label_pos.add(tuplepos)
	return all_label_block,used_label_pos,used_labels
	
#===> CDXML FILE GENERATION (using XML tag format) to open in ChemDraw
def cdxml_gen(profile_data,arg_array,gen_labels=True):
	'''Build the CDXML file containing the reaction profile plot from a list of DataFrames
	containing individual mechanisms and a parameter-containing object for plotting details.
	Input:
	- profile_data. ProfileData object possibly containing several mechanisms grouped by PATHWAY field.
	- arg_array. Initialized ParameterContainer
	- gen_labels. Boolean, if True include labels in the resulting plot.
	Output.
	- None.
	- Generates CDXML file.
	'''
	# Handle the output file
	outname = arg_array.filename[:-4] + ".cdxml"
	print("Output filename:",outname)
	fname = open(outname,"w")
	#Counters for IDs and colors  
	ii = 1
	colornumber = 4  # 1-> background 2-> background 3-> foreground
	# Start the main CDXML block:
	block = ''
	block += ("<CDXML>\n") 
	## Colortable from Matplotlib & custom colors if requested!
	if (arg_array.colorlist != ""):
		rgb_list = arg_array.colorlist.split(",")
	else:
		rgb_list = []
	cmapx = matplotlib.cm.get_cmap(arg_array.cmap)
	block += colortable_mpl(cmap=cmapx,custom_colors=rgb_list)
	# Page tag, with height and width
	block += ("<page\n")
	block += ("HeightPages=\"1\"\n")
	block += ("WidthPages=\"2\"\n")
	block += (">\n")
	fname.write(block)
	### Define used_label_pos as empty set before entering the loop for consistency in the 1st iteration
	used_label_pos = set()
	used_line_pos = set()
	used_labels = []
	line_color_dict = {}

	mechanisms_list = profile_data.pathway_splitter()
	for mech in mechanisms_list:
		current_color = colornumber
		colornumber += 1
		xy_array,label_list = xy_array_gen(mech,arg_array.line_spacing)
		n_entries = 2*len(mech)
		print("Starting fragment (mechanism). ID=%d" % ii)
		fname.write("<fragment\n")
		fname.write(" id=\"%d\"\n" % ii)
		fname.write(">\n")
		ii += 1
		#Every entry becomes a node in the CDXML file, there are NODES - 1 bonds (lines)
		list_indices = []
		print("Writing node tags.")
		nodelist,ii = node_iterator(xy_array,arg_array,ii)
		tagblock = node_bunch_tagger(nodelist)
		fname.write(tagblock)
		print(nodelist)
		#Bond (line) generation: use a flag to distinguish lines for species (TRUE) or connections (FALSE)
		species = True
		print("Writing line tags.")
		tagblock = ''
		for j in range(n_entries):
			try:
				begin = nodelist[j][0]
				end = nodelist[j+1][0]
			except:
				# Catching exception in the last node
				print("Reached last node in the mechanism")
				print("===========")
				continue
			# Generate the tags and track the lines for species and connectors, which must be alternated
			# Track pre-drawn lines: keep drawing (for consistency) but if present repeat the original color
			# Use the midpoint of each line as the tracking variable
			xm,ym = [round(0.5*(nodelist[j][k] + nodelist[j+1][k]),3) for k in [1,2]]
			if ((xm,ym) not in used_line_pos):
				# Track the line and the color
				used_line_pos.add((xm,ym))
				line_color_dict[(xm,ym)] = current_color
				line_color = current_color
			else:
				line_color = line_color_dict[(xm,ym)]
			
			tagblock += line_block(ii,begin,end,line_color,species)
			if (species):
				species = False
				print("Line %d-%d: %s (%.2f)" % (begin,end,label_list[j],xy_array[j,1]))
			else:
				species = True
			ii += 1
		tagblock += ("</fragment>\n")
		fname.write(tagblock)
		# Generation of labels:
		all_label_block,used_label_pos,used_labels = label_iterator(xy_array,arg_array,label_list,current_color,
																	used_label_pos,arg_array.skip_labels,
																	used_labels)
		fname.write(all_label_block)
	#Finish tags and close the file
	fname.write("</page></CDXML>\n")
	fname.close()
	return None

###Add SVG file generation!
def mpl_plotter(profile_data,arg_array,figsize=(6,6),fname=None,spc_labels=True,
				e_labels=True,showflag=True,ax=None,label_yshift_ratio=0.01):
	'''Draw the reaction profile via matplotlib, allowing to save as SVG file and also
	to show immediately as MPL session. Uses list of ProfileData elements containing individual mechanisms and initialize
	an interactive Matplotlib window to displace labels as desired..
	Input:
	- profile_data. ProfileData object possibly containing several mechanisms grouped by PATHWAY field.
	- arg_array. ParameterContainer() object
	- figsize. Tuple of integers, size of the canvas in inches.
	- fname. String, name of the SVG file. If None, no file is saved.
	- spc_labels. Boolean, if True, add labels for species.
	- e_labels. Boolean, if True, add labels for energies.
	- showflag. Boolean, if True, show figure in Matplotlib window.
	'''
	
	# Process mechanism list and start iteration
	mechanisms_list = profile_data.pathway_splitter()
	cmapx = matplotlib.cm.get_cmap(arg_array.cmap)

	Ncolors = len(mechanisms_list)
	color_list = [cmapx(xval) for xval in np.linspace(0,1,Ncolors)]
	# Add custom colors at the beginning of the list, if present
	if (arg_array.colorlist != ""):
		rgb_list = arg_array.colorlist.split(",")
		custom_colors = [color_formatter(color) for color in rgb_list]
		new_color_list = custom_colors + color_list
		color_list = new_color_list

	# Add this Py list to the ParameterContainer
	arg_array.colorlist_mpl = color_list	
	# Prepare throwaway lists
	used_labels = []
	used_energies = []

	# Get the max and min energies that will be in the plot to later get the y-shift for labels
	# (1% of span by default)
	ymin = profile_data.min()[1]
	ymax = profile_data.max()[1]
	yshift = label_yshift_ratio*(ymax - ymin)

	# Do this only if no ax is passed
	if (not ax):
		fig = plt.figure(figsize=figsize)
		ax = fig.add_subplot(111)
	else:
		fig = ax.get_figure()

	# iteration
	prev_positions = []
	for ii,mech in enumerate(mechanisms_list):
		current_used_labels = []
		current_used_energies = []
		current_color = color_list[ii]

		mech.sort_profile("Stage")

		# Prepare the plotting array and the axis information, then plot the skeleton
		xy_array,label_list = xy_array_gen(mech,arg_array.line_spacing)
		#Profile structure (colored per mechanism)
		ax.set_xlabel("Reac. coordinate")
		ax.set_ylabel("G/kcal mol$^{-1}$")
		ax.plot(xy_array[:,0],xy_array[:,1],'--',color=current_color,linewidth=arg_array.lw1)
		# Lines for species and labels
		for jj in range(len(mech)):
			# Draw wider lines for species
			node1 = xy_array[2*jj,:]
			node2 = xy_array[2*jj+1,:]
			ax.plot((node1[0],node2[0]),(node1[1],node2[1]),color=current_color,linewidth=arg_array.lw2)
			# Requires to give STRING, (x,y) input. Always add energy, check whether label was already used or not
			format_string = "%." + ("%df" % arg_array.digits)
			e_fmt = format_string % mech.Energy[jj]
			xmid = 0.5*(node1[0] + node2[0])
			position = np.array([xmid,node1[1]-yshift])
			overlaps = [np.linalg.norm(prev_pos - position) for prev_pos in prev_positions]
			overlap_flag = any([ov < 1e-4 for ov in overlaps])
			if (e_labels and not overlap_flag):
				ax.annotate(e_fmt,position,
							color=current_color,va='top',
							ha='center',picker=True).draggable()
				prev_positions.append(position)
				current_used_energies.append(mech.Energy[jj])
				
			if (mech.Label[jj] not in used_labels):
				if (spc_labels):
					ax.annotate(mech.Label[jj],position,
								ha="center",va="bottom").draggable()
					prev_positions.append(position)
					
				current_used_labels.append(mech.Label[jj])
		# Update used labels and energies in a per-mechanism basis, so if a label is repeated
		# in the same profile is still included
		used_labels.extend(current_used_labels)
		used_energies.append(current_used_labels)
		
	if (fname):
		outname = fname + "_unaltered.svg"
		plt.savefig(outname)
	#Only show interactive window if showflag is True
	if (showflag == True):
		plt.show()
	return fig,ax

def line_overlap(line1,line2):
	# check overlap of two segments [(x0,y0),(x1,y1)] and [(x2,y2),(x3,y3)]
	threshold = 1e-6
	diff1 = np.abs(line2[0] - line1[0]) < threshold
	diff2 = np.abs(line2[1] - line1[1]) < threshold
	return all(diff1) and all(diff2)

def mpl_plotter_clean(profile_data,arg_array,figsize=(6,6),spc_labels=True,
					  e_labels=True,ax=None,label_yshift_ratio=0.01,
					  repeat_labels=False):
	'''Draw the reaction profile via matplotlib, allowing to save as SVG file and also
	to show immediately as MPL session. Uses list of ProfileData elements containing individual mechanisms and initialize
	an interactive Matplotlib window to displace labels as desired..
	Input:
	- profile_data. ProfileData object possibly containing several mechanisms grouped by PATHWAY field.
	- arg_array. ParameterContainer() object
	- figsize. Tuple of integers, size of the canvas in inches.
	- fname. String, name of the SVG file. If None, no file is saved.
	- spc_labels. Boolean, if True, add labels for species.
	- e_labels. Boolean, if True, add labels for energies.
	- showflag. Boolean, if True, show figure in Matplotlib window.
	'''
	
	# Process mechanism list to know how many colors are needed
	mechanisms_list = profile_data.pathway_splitter()
	cmapx = matplotlib.cm.get_cmap(arg_array.cmap)

	Ncolors = len(mechanisms_list)
	color_list = [cmapx(xval) for xval in np.linspace(0,1,Ncolors)]
	
	# Add custom colors at the beginning of the list, if present
	if (arg_array.colorlist != ""):
		rgb_list = arg_array.colorlist.split(",")
		custom_colors = [color_formatter(color) for color in rgb_list]
		new_color_list = custom_colors + color_list
		color_list = new_color_list

	# Add this Py list to the ParameterContainer
	arg_array.colorlist_mpl = color_list
	
	# Prepare throwaway lists
	used_labels = []
	used_energies = []

	# Get the max and min energies that will be in the plot to later get the y-shift for labels
	# (1% of span by default)
	ymin = profile_data.min()[1]
	ymax = profile_data.max()[1]
	yshift = label_yshift_ratio*(ymax - ymin)

	# Do this only if no ax is passed
	if (not ax):
		fig = plt.figure(figsize=figsize)
		ax = fig.add_subplot(111)
	else:
		fig = ax.get_figure()

	# Main iteration
	prev_positions_labels = []
	prev_positions_entries = []
	
	for ii,mech in enumerate(mechanisms_list):
		current_used_labels = []
		current_used_energies = []
		current_prev_positions = []
		all_positions = []
		current_sequence = []
		current_color = color_list[ii]


		mech.sort_profile("Stage")

		# Prepare the plotting array and the axis information, then plot the skeleton
		xy_array,label_list = xy_array_gen(mech,arg_array.line_spacing)
		
		#Profile structure (colored per mechanism)
		ax.set_xlabel("Reac. coordinate")
		ax.set_ylabel("G/kcal mol$^{-1}$")
		# Skeleton dashed line plot
		#ax.plot(xy_array[:,0],xy_array[:,1],'--',color=current_color,linewidth=arg_array.lw1)
		# Lines for species and labels
		for jj in range(len(mech)):
			# Draw wider lines for species
			node1 = xy_array[2*jj,:]
			node2 = xy_array[2*jj+1,:]
			# Check whether it overlaps before drawing it
			pos_line = [node1,node2]
			all_positions.append(pos_line)
			overlaps_line = [line_overlap(pos_line,prev_pos) for prev_pos in prev_positions_entries]
			if (not any(overlaps_line)):
				ax.plot((node1[0],node2[0]),(node1[1],node2[1]),color=current_color,linewidth=arg_array.lw2)
				current_prev_positions.append(pos_line)
				current_sequence.append(jj)
			else:
				continue
			# Requires to give STRING, (x,y) input. Always add energy, check whether label was already used or not
			format_string = "%." + ("%df" % arg_array.digits)
			e_fmt = format_string % mech.Energy[jj]
			xmid = 0.5*(node1[0] + node2[0])
			position_lab = np.array([xmid,node1[1]-yshift])
			overlaps_lab = [np.linalg.norm(prev_pos - position_lab) for prev_pos in prev_positions_labels]
			overlap_flag = any([ov < 1e-4 for ov in overlaps_lab])
			if (e_labels and not overlap_flag):
				ax.annotate(e_fmt,position_lab,
							color=current_color,va='top',
							ha='center',picker=True).draggable()
				prev_positions_labels.append(position_lab)
				current_used_energies.append(mech.Energy[jj])
			new_label = mech.Label[jj] not in used_labels
			include_label = new_label or repeat_labels
			if (include_label):
				if (spc_labels):
					ax.annotate(mech.Label[jj],position_lab,
								ha="center",va="bottom").draggable()
					prev_positions_labels.append(position_lab)
					
				current_used_labels.append(mech.Label[jj])
		# Draw the skeleton dashed lines ONLY for the included lines
		# get the pairs
		Nelems = len(all_positions)
		segment_list = []
		for idx,line in zip(current_sequence,current_prev_positions):
			add_segments = []
			if (idx > 0):
				# backwards connection
				add_segments.append([all_positions[idx-1][1],all_positions[idx][0]])
			if (idx < Nelems - 1):
				# forwards connection
				add_segments.append([all_positions[idx][1],all_positions[idx+1][0]])
			segment_list += add_segments	
		
		if (segment_list):
			linecol = matplotlib.collections.LineCollection(segment_list,color=current_color,
														linewidth=arg_array.lw1,linestyle="dashed")

			ax.add_collection(linecol)
		#ax.plot(xy_array[:,0],xy_array[:,1],'--',color=current_color,linewidth=arg_array.lw1)
		# Update used labels and energies in a per-mechanism basis, so if a label is repeated
		# in the same profile is still included
		prev_positions_entries.extend(current_prev_positions)
		used_labels.extend(current_used_labels)
		used_energies.append(current_used_labels)
		
	return fig,ax
